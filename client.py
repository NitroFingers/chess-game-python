import socket
import json
import random
import time
import threading
import pygame
from messageDialog import MessageDialog

# Server Info, needs editing
servers = [['CHILL-PC', 25565],
           ['localhost', 25565],
           ['169.255.197.38', 25565]]

class ServerThread(threading.Thread): # 
    def __init__(self, client=None):
        threading.Thread.__init__(self)
        self.client = client
        self.s = self.client.s

    def run(self):
        while self.client.running:
            data = self.s.recv(1024)
            if not data: break

            command = json.loads(data)
            if command['action'] == 'joinableRoom':
                print('Room created')
                self.client.menu.addNewRoomToJoin(name=command['roomName'])
            if command['action'] == 'makeMove':
                From = (command['from'][0], command['from'][1])
                to = (command['to'][0], command['to'][1])

                From = (7 - From[0], 7 - From[1])
                to = (7 - to[0],7 - to[1])

                self.client.board.makeMove(From, to)
            if command['action'] == 'setCanMove':
                print('setCanMove')
                self.client.board.canMove = command['canMove']
                if command['canMove']:
                    #pygame.display.set_caption('White')
                    self.client.board.loadWhite()
                    self.client.md.close()
                else:
                    #pygame.display.set_caption('Black')
                    self.client.board.loadBlack()
                print('setCanMove done')


            if command['action'] == 'roomClosed':
                self.client.app.room_close()
            if command['action'] == 'joinRoom':
                self.client.app.start_joined_room()

class Client:
    def __init__(self, menu=None, app=None):
        self.HOST = None
        self.PORT = None
        self.s = None
        self.running = True
        self.serverthread = None
        self.menu = menu
        self.board = None
        self.app = app

        self.md = None

    def stop(self):
        self.s.close()

    def connect(self):
        connected = False
        print('In function connect')

        for server in servers:
            try:
                print('Connecting to ', server)
                time.sleep(0.1)
                self.HOST = server[0]
                self.PORT = server[1]
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.s.connect((self.HOST, self.PORT))
                connected = True
                break
            except Exception as e:
                print(server, ' innaccesible ', e)



        if not connected:
            print('Could''nt connect to any server.')
            quit()
        else:
            print('Connected!')


        self.serverthread = ServerThread(client=self)
        self.serverthread.start()

        newConnection = {
                'action' : 'connect',
        }
        data = json.dumps(newConnection)
        self.s.sendall(data.encode('utf-8'))

    def createRoom(self):
        roomName = 'room' + str(random.randrange(0,10000))
        createRoom = {
                'action' : 'createRoom',
                'roomName' : roomName
        }
        data = json.dumps(createRoom)
        pygame.display.set_caption(roomName)
        self.s.sendall(data.encode('utf-8'))

        self.md = MessageDialog(app=self.app, caption='Awaiting opponent...', text='Game wil start when opponent joins room.', size=(400,130), canClose=False)

        self.app.objects.append(self.md)

    def request_joinable_rooms(self):
        rqjr = {
                'action' : 'request_joinable_rooms',
        }
        data = json.dumps(rqjr)
        self.s.sendall(data.encode('utf-8'))

    def requestJoinRoom(self, room):
        rjr = {
                'action' : 'requestJoinRoom',
                'roomName' : room
        }
        data = json.dumps(rjr)
        self.s.sendall(data.encode('utf-8'))


    def makeMove(self, From=None, to=None):
        createRoom = {
                'action' : 'makeMove',
                'from' : From,
                'to'   : to

        }
        data = json.dumps(createRoom)
        self.s.sendall(data.encode('utf-8'))
