##
## CHECK and CHECKMATE
##
## ENPARSANT
##

import pygame
from pygame.draw import rect
from pygame.locals import *
from time import time, sleep
from random import randrange
import math

chessImages = pygame.image.load('pieces.png')
chessImages = pygame.transform.scale(chessImages, (300,100))
ITS = {
        'king'  : 0,
        'queen' : 50,
        'bishop': 100,
        'knight': 150,
        'castle': 200,
        'pawn'  : 250,
        True    : 0,
        False   : 50
}

class Piece:
    def __init__(self, type=(True,'pawn'), pos=(0,0)):
        self.type = type
        self.imageInfo = (ITS[self.type[1]],ITS[self.type[0]],50,50)
        self.pos = pos
        self.firstMove = True
        self.possibleMoves = []

    def setPos(self,pos):
        self.pos = pos
        self.firstMove = False

    def selected(self, surface, board):
        self.possibleMoves = []
        pos = self.pos
        rect(surface, (55,55,255), (pos[0] * 50, pos[1] * 50, 50, 50))

        #Pawn
        if (self.type[1] == 'pawn'):
            if board.getPiece((pos[0],pos[1]-1)) == None:
                self.possibleMoves.append((pos[0],pos[1]-1))
            if board.getPiece((pos[0]+1,pos[1]-1)) != None:
                self.possibleMoves.append((pos[0]+1,pos[1]-1))
            if board.getPiece((pos[0]-1,pos[1]-1)) != None:
                self.possibleMoves.append((pos[0]-1,pos[1]-1))
            if self.firstMove:
                if board.getPiece((pos[0],pos[1]-2)) == None:
                    self.possibleMoves.append((pos[0],pos[1]-2))
        ##knight
        if (self.type[1] == 'knight'):
            self.possibleMoves.append((pos[0] + 1,pos[1] + 2))
            self.possibleMoves.append((pos[0] - 1,pos[1] + 2))
            self.possibleMoves.append((pos[0] + 1,pos[1] - 2))
            self.possibleMoves.append((pos[0] - 1,pos[1] - 2))

            self.possibleMoves.append((pos[0] + 2,pos[1] + 1))
            self.possibleMoves.append((pos[0] - 2,pos[1] + 1))
            self.possibleMoves.append((pos[0] + 2,pos[1] - 1))
            self.possibleMoves.append((pos[0] - 2,pos[1] - 1))
        ##king
        if (self.type[1] == 'king'):
            self.possibleMoves.append((pos[0] + 1,pos[1]))
            self.possibleMoves.append((pos[0] - 1,pos[1]))
            self.possibleMoves.append((pos[0],pos[1] + 1))
            self.possibleMoves.append((pos[0],pos[1] - 1))

            self.possibleMoves.append((pos[0] + 1,pos[1] + 1))
            self.possibleMoves.append((pos[0] - 1,pos[1] - 1))
            self.possibleMoves.append((pos[0] - 1,pos[1] + 1))
            self.possibleMoves.append((pos[0] + 1,pos[1] - 1))

            if self.firstMove:
                if self.type[0]: ##WHITE
                    if (board.getPiece((0,pos[1])) != None) and (board.getPiece((pos[0] - 2,pos[1])) == None):
                        if board.getPiece((0,pos[1])).firstMove:
                            if (board.getPiece((1,pos[1])) == None) and (board.getPiece((2,pos[1])) == None) and (board.getPiece((3,pos[1])) == None):
                                self.possibleMoves.append((pos[0] - 2,pos[1]))

                    if (board.getPiece((7,pos[1])) != None):
                        if board.getPiece((7,pos[1])).firstMove:
                            if (board.getPiece((5,pos[1])) == None) and (board.getPiece((6,pos[1])) == None):
                                self.possibleMoves.append((pos[0] + 2,pos[1]))
                else:
                    if (board.getPiece((7,pos[1])) != None):
                        if board.getPiece((7,pos[1])).firstMove:
                            if (board.getPiece((4,pos[1])) == None) and (board.getPiece((5,pos[1])) == None) and (board.getPiece((6,pos[1])) == None):
                                self.possibleMoves.append((pos[0] + 2,pos[1]))

                    if (board.getPiece((0,pos[1])) != None):
                        if board.getPiece((0,pos[1])).firstMove:
                            if (board.getPiece((1,pos[1])) == None) and (board.getPiece((2,pos[1])) == None):
                                self.possibleMoves.append((pos[0] - 2,pos[1]))

        ##bishop
        if (self.type[1] == 'bishop'):
            d = 1
            while (((d + pos[0]) < 8) and ((d + pos[1]) < 8)):
                if board.getPiece((pos[0] + d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                    break
                d += 1
            d = -1
            while (((d + pos[0]) > -1) and ((d + pos[1]) > -1)):
                if board.getPiece((pos[0] + d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                    break
                d -= 1

            d = 1

            d = 1
            while (((pos[0] + d) < 8) and ((pos[1] - d) > -1)):
                if board.getPiece((pos[0] + d,pos[1] - d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] - d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] - d))
                    break
                d += 1
            d = 1
            while (((pos[0] - d) > -1) and ((pos[1] + d) < 8)):
                if board.getPiece((pos[0] - d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] - d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] - d,pos[1] + d))
                    break
                d += 1
        ##castle
        if (self.type[1] == 'castle'):
            for x in range(pos[0] + 1, 8):
                if board.getPiece((x,pos[1])) == None:
                    self.possibleMoves.append((x,pos[1]))
                else:
                    self.possibleMoves.append((x,pos[1]))
                    break
            for x in range(pos[0] - 1, -1, -1):
                if board.getPiece((x,pos[1])) == None:
                    self.possibleMoves.append((x,pos[1]))
                else:
                    self.possibleMoves.append((x,pos[1]))
                    break
            for y in range(pos[1] + 1, 8):
                if board.getPiece((pos[0],y)) == None:
                    self.possibleMoves.append((pos[0],y))
                else:
                    self.possibleMoves.append((pos[0],y))
                    break
            for y in range(pos[1] - 1, -1,-1):
                if board.getPiece((pos[0],y)) == None:
                    self.possibleMoves.append((pos[0],y))
                else:
                    self.possibleMoves.append((pos[0],y))
                    break
        ##queen
        if (self.type[1] == 'queen'):
            for x in range(pos[0] + 1, 8):
                if board.getPiece((x,pos[1])) == None:
                    self.possibleMoves.append((x,pos[1]))
                else:
                    self.possibleMoves.append((x,pos[1]))
                    break
            for x in range(pos[0] - 1, -1, -1):
                if board.getPiece((x,pos[1])) == None:
                    self.possibleMoves.append((x,pos[1]))
                else:
                    self.possibleMoves.append((x,pos[1]))
                    break
            for y in range(pos[1] + 1, 8):
                if board.getPiece((pos[0],y)) == None:
                    self.possibleMoves.append((pos[0],y))
                else:
                    self.possibleMoves.append((pos[0],y))
                    break
            for y in range(pos[1] - 1, -1,-1):
                if board.getPiece((pos[0],y)) == None:
                    self.possibleMoves.append((pos[0],y))
                else:
                    self.possibleMoves.append((pos[0],y))
                    break

            d = 1
            while (((d + pos[0]) < 8) and ((d + pos[1]) < 8)):
                if board.getPiece((pos[0] + d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                    break
                d += 1
            d = -1
            while (((d + pos[0]) > -1) and ((d + pos[1]) > -1)):
                if board.getPiece((pos[0] + d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] + d))
                    break
                d -= 1

            d = 1

            d = 1
            while (((pos[0] + d) < 8) and ((pos[1] - d) > -1)):
                if board.getPiece((pos[0] + d,pos[1] - d)) == None:
                    self.possibleMoves.append((pos[0] + d,pos[1] - d))
                else:
                    self.possibleMoves.append((pos[0] + d,pos[1] - d))
                    break
                d += 1
            d = 1
            while (((pos[0] - d) > -1) and ((pos[1] + d) < 8)):
                if board.getPiece((pos[0] - d,pos[1] + d)) == None:
                    self.possibleMoves.append((pos[0] - d,pos[1] + d))
                else:
                    self.possibleMoves.append((pos[0] - d,pos[1] + d))
                    break
                d += 1





        newPpos = []
        for ppos in self.possibleMoves:
            if not (board.getPiece(ppos) == None):
                if board.getPiece(ppos).type[0] != self.type[0]:
                    newPpos.append(ppos)
            else:
                newPpos.append(ppos)
        self.possibleMoves = newPpos

        bSize = 20
        bbSize = 18
        for ppos in self.possibleMoves:
            if board.getPiece(ppos) == None:
                rect(surface, (0,0,0), (ppos[0] * 50 + bbSize, (ppos[1]) * 50 + bbSize, 50 - (bbSize * 2), 50 - (bbSize * 2)))
                rect(surface, (0,0,255), (ppos[0] * 50 + bSize, (ppos[1]) * 50 + bSize, 50 - (bSize * 2), 50 - (bSize * 2)))
            else:
                rect(surface, (255,0,0), (ppos[0] * 50 + bSize, (ppos[1]) * 50 + bSize, 50 - (bSize * 2), 50 - (bSize * 2)))
    def draw(self, surface):
        surface.blit(chessImages, (self.pos[0] * 50, self.pos[1] * 50), self.imageInfo)


class Board:
    def __init__(self, status=None, client=None):
        print('Initializing board...')

        self.size = (400,400)
        self.pieces = []
        self.selectedPiece = None

        self.surface = pygame.surface.Surface(self.size)
        #self.cleanBoard()

        self.nextTurn = True

        self.status = status
        self.client = client
        self.status.setTurn(self.nextTurn)

        self.canMove = None
        self.reDraw()

    def reDraw(self):
        self.surface.fill((0,0,0))
        blockSize = self.size[0] / 8

        bl = True

        blackSquere = (20,100,20) #(55,200,55)
        whiteSquere = (250,250,250) #(200,55,55)

        for x in range(0,8):
            for y in range(0,8):
                if bl:
                    rect(self.surface, whiteSquere, (x * blockSize, y * blockSize, blockSize, blockSize))
                else:
                    rect(self.surface, blackSquere, (x * blockSize, y * blockSize, blockSize, blockSize))
                bl = not bl
            bl = not bl

        for piece in self.pieces:
            piece.draw(self.surface)

    def loadWhite(self):
        for i in range(0,8):
            self.pieces.append(Piece(type=(False,'pawn'), pos=(i,1)))
            self.pieces.append(Piece(type=(True,'pawn'), pos=(i,6)))

        self.pieces.append(Piece(type=(False,'king'), pos=(4,0)))
        self.pieces.append(Piece(type=(True,'king'), pos=(4,7)))
        self.pieces.append(Piece(type=(False,'queen'), pos=(3,0)))
        self.pieces.append(Piece(type=(True,'queen'), pos=(3,7)))

        self.pieces.append(Piece(type=(False,'castle'), pos=(0,0)))
        self.pieces.append(Piece(type=(False,'castle'), pos=(7,0)))
        self.pieces.append(Piece(type=(True,'castle'), pos=(0,7)))
        self.pieces.append(Piece(type=(True,'castle'), pos=(7,7)))

        self.pieces.append(Piece(type=(False,'bishop'), pos=(2,0)))
        self.pieces.append(Piece(type=(False,'bishop'), pos=(5,0)))
        self.pieces.append(Piece(type=(True,'bishop'), pos=(2,7)))
        self.pieces.append(Piece(type=(True,'bishop'), pos=(5,7)))

        self.pieces.append(Piece(type=(False,'knight'), pos=(1,0)))
        self.pieces.append(Piece(type=(False,'knight'), pos=(6,0)))
        self.pieces.append(Piece(type=(True,'knight'), pos=(1,7)))
        self.pieces.append(Piece(type=(True,'knight'), pos=(6,7)))

        self.reDraw()

    def loadBlack(self):

        print('loading pawns')
        for i in range(0,8):
            self.pieces.append(Piece(type=(True,'pawn'), pos=(i,1)))
            self.pieces.append(Piece(type=(False,'pawn'), pos=(i,6)))

        print('loading kings and queens')
        self.pieces.append(Piece(type=(True,'king'), pos=(3,0)))
        self.pieces.append(Piece(type=(False,'king'), pos=(3,7)))
        self.pieces.append(Piece(type=(True,'queen'), pos=(4,0)))
        self.pieces.append(Piece(type=(False,'queen'), pos=(4,7)))

        print('loading castles')
        self.pieces.append(Piece(type=(True,'castle'), pos=(0,0)))
        self.pieces.append(Piece(type=(True,'castle'), pos=(7,0)))
        self.pieces.append(Piece(type=(False,'castle'), pos=(0,7)))
        self.pieces.append(Piece(type=(False,'castle'), pos=(7,7)))

        print('loading bishops')
        self.pieces.append(Piece(type=(True,'bishop'), pos=(2,0)))
        self.pieces.append(Piece(type=(True,'bishop'), pos=(5,0)))
        self.pieces.append(Piece(type=(False,'bishop'), pos=(2,7)))
        self.pieces.append(Piece(type=(False,'bishop'), pos=(5,7)))

        print('loading knights')
        self.pieces.append(Piece(type=(True,'knight'), pos=(1,0)))
        self.pieces.append(Piece(type=(True,'knight'), pos=(6,0)))
        self.pieces.append(Piece(type=(False,'knight'), pos=(1,7)))
        self.pieces.append(Piece(type=(False,'knight'), pos=(6,7)))

        print('Attempting redraw...')
        self.reDraw()


    def update(self):
        pass

    def getPiece(self, pos):
        for piece in self.pieces:
            if piece.pos == pos:
                return piece
        return None

    def makeMove(self, From, to):
        if self.getPiece(to) != None:
            self.pieces.remove(self.getPiece(to))

        if self.getPiece(From).firstMove:
            if self.getPiece(From).type[1] == 'king':
                if self.canMove: #WHITE BOTTEM BOARD CASTLE
                    if (to == (self.getPiece(From).pos[0] - 2,self.getPiece(From).pos[1])):
                        #self.makeMove((0,self.getPiece(From).pos[1]),(3,self.getPiece(From).pos[1]))
                        self.getPiece((0,self.getPiece(From).pos[1])).pos = (3,self.getPiece(From).pos[1])
                    elif (to == (self.getPiece(From).pos[0] + 2,self.getPiece(From).pos[1])):
                        #self.makeMove((7,self.getPiece(From).pos[1]),(5,self.getPiece(From).pos[1]))
                        self.getPiece((7,self.getPiece(From).pos[1])).pos = (5,self.getPiece(From).pos[1])
                else:  #BLACK BOTTEM BOARD CASTLE
                    if (to == (self.getPiece(From).pos[0] - 2,self.getPiece(From).pos[1])):
                        #self.makeMove((0,self.getPiece(From).pos[1]),(3,self.getPiece(From).pos[1]))
                        self.getPiece((0,self.getPiece(From).pos[1])).pos = (2,self.getPiece(From).pos[1])
                    elif (to == (self.getPiece(From).pos[0] + 2,self.getPiece(From).pos[1])):
                        #self.makeMove((7,self.getPiece(From).pos[1]),(5,self.getPiece(From).pos[1]))
                        self.getPiece((7,self.getPiece(From).pos[1])).pos = (4,self.getPiece(From).pos[1])

        self.getPiece(From).setPos(to)

        self.nextTurn = not self.nextTurn
        self.status.setTurn(self.nextTurn)
        self.reDraw()

    def eventMouseUp(self, pos):
        pos = (pos[0] - 10, pos[1] - 10)
        pos = (math.floor(pos[0] / 50), math.floor(pos[1] / 50))

        if ((pos[0] >= 0) and (pos[1] >= 0)):
            if ((pos[0] <= 7) and (pos[1] <= 7)):
                if self.selectedPiece == None:
                    piece = self.getPiece(pos)
                    if piece:
                        if (piece.type[0] == self.nextTurn) and (piece.type[0] == self.canMove):
                            print('selectedPiece=' + str(piece.type))
                            self.selectedPiece = piece

                            piece.selected(self.surface, self)

                            piece.draw(self.surface)
                        else:
                            self.selectedPiece = None
                    else:
                        print('no piece')
                else:
                    if pos in self.selectedPiece.possibleMoves:
                        self.client.makeMove(From=self.selectedPiece.pos, to=pos)

                        self.makeMove(From=self.selectedPiece.pos, to=pos)

                        self.selectedPiece = None
                    else:
                        print('Impossible move... selected = None')
                        self.selectedPiece = None
                        self.reDraw()
                return True

    def draw(self, surface):
        surface.blit(self.surface, (10,10))
