import socket
import time
import threading
import json

class GameRoom: # Object that can accept two players and sends data between clients
    def __init__(self, name='roomUnknown'):
        self.white = None
        self.black = None
        self.name = name

    def close(self, client):
        closeRoom = {
                'action' : 'roomClosed'

        }
        data = json.dumps(closeRoom).encode('utf-8')
        if self.white == client:
            self.black.send(data)
        else:
            self.white.send(data)

    def joinable(self): #joinable is false when a client can join
        if self.white.connected and (self.black == None):
            return True
        return False

    def setWhite(self, client=None):
        self.white = client

    def setBlack(self, client=None):
        self.black = client
        canMove = {
                'action' : 'setCanMove',
                'canMove' : True,
        }
        data = json.dumps(canMove).encode('utf-8')
        self.white.send(data)

        canMove = {
                'action' : 'setCanMove',
                'canMove' : False,
        }
        data = json.dumps(canMove).encode('utf-8')
        self.black.send(data)

    def makeMove(self, client=None, From=None, to=None):
        createRoom = {
                'action' : 'makeMove',
                'from' : From,
                'to'   : to

        }
        data = json.dumps(createRoom).encode('utf-8')
        if self.white == client:
            self.black.send(data)
        else:
            self.white.send(data)


# This thread waits for a client to connect and handles input from client
class ClientThread(threading.Thread):
    def __init__(self, server):
        threading.Thread.__init__(self)
        self.server = server
        self.server.clients.append(self)
        self.connected = False

        self.myRoom = None

    def joinRoom(self, room):
        jr = {
                'action' : 'joinRoom',
                'roomName' : room.name
        }
        self.send(json.dumps(jr).encode('utf-8'))

    def run(self):
        self.conn, self.addr = self.server.s.accept() # Waits for client to connect
        print(self.addr, ' connected to ', self)
        self.connected = True
        self.server.newCThread()

        while self.server.running:
            data = self.conn.recv(1024)
            if not data: break #Usually disconection or broken socket

            command = json.loads(data) #unpacks recieved data
            if command['action'] == 'createRoom':
                self.myRoom = self.server.newRoom(command['roomName'])
                self.myRoom.setWhite(self)
            if command['action'] == 'makeMove':
                self.myRoom.makeMove(client=self, From=command['from'], to=command['to'])
            if command['action'] == 'requestJoinRoom':
                for room in self.server.rooms:
                    if room.name == command['roomName']:
                        if room.joinable():
                            self.joinRoom(room)
                            time.sleep(0.1)
                            room.setBlack(self)
                            self.myRoom = room
                            print(self, ' joined room ', room)
                            break
            if command['action'] == 'request_joinable_rooms':
                for room in self.server.rooms:
                    if room.joinable():
                        time.sleep(0.1)
                        newRoom = {
                                'action' : 'joinableRoom',
                                'roomName' : room.name
                        }
                        self.send(json.dumps(newRoom).encode('utf-8'))

        self.connected = False
        if self.myRoom != None:
            self.myRoom.close(self)
        self.server.clients.remove(self)
        print(self, ' end')


    def send(self, data):
        self.conn.send(data)



class Server:
    def __init__(self):
        self.HOST = ''
        self.PORT = 25565

        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        print('Starting server...')
        self.running = False
        while not self.running:
            try:
                self.s.bind((self.HOST, self.PORT))
                self.running = True
            except:
                self.PORT += 1
        print('Server started on port ', self.PORT)
        self.s.listen(128) # Soft limit to 128 clients

        self.rooms = []

        self.clients = []
        self.cThreads = []

        self.newCThread()

    def newCThread(self): #Starts new thread that waits for connection from client
        print('spawning new thread')
        nct = ClientThread(self)
        nct.start()
        self.cThreads.append(nct)

    def newRoom(self, name='roomUnknown'):
        room = GameRoom(name=name)
        self.rooms.append(room)
        print('Room created')
        newRoom = {
                'action' : 'addRoom',
                'roomName' : name
        }
        #self.sendAll(newRoom)

        return room

    def sendAll(self, data): #Broadcasts data to ALL connected clients
        for client in self.clients:
            if client.connected:
                client.send(json.dumps(data).encode('utf-8'))

    def loop(self):
        print('Available commands:')
        print(' - list : Prints all connected clients and empty threads')
        while self.running:
            try:
                i = input('=>')
                if i == 'list':
                    for client in self.clients:
                        print(client, client.connected)
            except:
                self.running = False
                for thrd in self.cThreads:
                    thrd.join()
                print('Joining threads...')

s = Server()
s.loop()
