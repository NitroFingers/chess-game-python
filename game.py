import pygame
from pygame.draw import rect
from pygame.locals import *
from time import time, sleep
from random import randrange
from board import Board
from status import Status
from menu import Menu
from client import Client
from messageDialog import MessageDialog



class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 590, 420

        self.objects = []

        self.status = None
        self.board = None
        self.menu = Menu(app=self)
        self.menu.mainMenuSetup()
        self.client = Client(menu=self.menu, app=self)

        self.objects.append(self.menu)


    def room_close(self):
        self.board = None
        self.objects = []
        self.objects.append(self.menu)
        md = MessageDialog(app=self, caption='Room Closed', text='Your opponent has disconnected.')
        self.objects.append(md)
        self.menu.mainMenuSetup(gameDisconnect=True)


    def start_empty_room(self):
        self.objects.remove(self.menu)

        self.status = Status()
        self.board = Board(status=self.status, client=self.client)
        self.client.board = self.board

        self.objects.append(self.board)
        self.objects.append(self.status)


    def start_joined_room(self):
        self.objects = []

        self.status = Status()
        self.board = Board(status=self.status, client=self.client)
        self.client.board = self.board

        self.objects.append(self.board)
        self.objects.append(self.status)


    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True


    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.KEYDOWN:
            if event.key == 273:
                pass
            if event.key == 274:
                pass
        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()


            for i in range(len(self.objects) - 1, -1, -1):
                if self.objects[i].eventMouseUp(pos):
                    break


    def on_loop(self):
        for obj in self.objects:
            obj.update()

    def on_render(self):
        self._display_surf.fill((100,100,100))
        for obj in self.objects:
            obj.draw(self._display_surf)
        pygame.display.update()

    def on_cleanup(self):
        self.client.stop()
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False

        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()

if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()
