class Wall:
    def __init__(self):
        self.pos = (640, randrange(100, 300))
        self.v = -100
        self.gapSize = 150
        self.width = 15

    def rect1(self):
        return (self.pos[0], 0, self.width, self.pos[1] - (self.gapSize / 2))
    def rect2(self):
        return (self.pos[0], self.pos[1] + (self.gapSize / 2), self.width, 400)

    def draw(self, surface):
        rect(surface, (0,0,255), self.rect1())
        rect(surface, (0,0,255), self.rect2())

    def update(self, deltaTime):
        self.pos = (self.pos[0] + (self.v * deltaTime), self.pos[1])

        if self.pos[0] <= (self.width * -1):
            return True
        else:
            return False
