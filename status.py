import pygame
from pygame.draw import rect
from pygame.locals import *
from time import time, sleep
from random import randrange
import math

chessImages = pygame.image.load('pieces.png')
chessImages = pygame.transform.scale(chessImages, (300,100))
ITS = {
        'king'  : 0,
        'queen' : 50,
        'bishop': 100,
        'knight': 150,
        'castle': 200,
        'pawn'  : 250,
        True    : 0,
        False   : 50
}

class Status:
    def __init__(self):
        self.size = (160,400)
        self.surface = pygame.surface.Surface(self.size)
        self.turn = True
        self.iiBlack = (ITS['king'],ITS[False],50,50)
        self.iiWhite = (ITS['king'],ITS[True],50,50)

    def setTurn(self, turn):
        self.turn = turn
        self.reDraw()

    def reDraw(self):
        self.surface.fill((55,55,200))
        if self.turn:
            self.surface.blit(chessImages, (0, 0), self.iiWhite)
        else:
            self.surface.blit(chessImages, (0,0), self.iiBlack)

    def update(self):
        pass

    def draw(self, surface):
        surface.blit(self.surface, (420,10))

    def eventMouseUp(self, pos):
        return False
