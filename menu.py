# LIST OF TAKEN PIECES
#
#
#

import pygame
from pygame.draw import rect
from pygame.locals import *
from time import time, sleep
from random import randrange
import math

mFont = None

def inRect(pos, rect):
    if (pos[0] > rect[0]) and (pos[0] < (rect[0] + rect[2])):
        if (pos[1] > rect[1]) and (pos[1] < (rect[1] + rect[3])):
            return True
    return False

class Button():
    def __init__(self, text='None', action=None, pos=(10,10), parameter=None):
        self.text = text
        self.action = action
        self.parameter = parameter
        self.pos = pos
        self.tSurface = mFont.render(text, False, (0, 0, 0))
        self.rect = (self.pos[0], self.pos[1],self.tSurface.get_width(), self.tSurface.get_height())

    def draw(self, surface):
        rect(surface, (0,200,0), self.rect)

        surface.blit(self.tSurface, self.pos)

    def checkPress(self, pos):
        if inRect(pos, self.rect):
            if self.action != None:
                if self.parameter == None:
                    self.action()
                else:
                    self.action(self.parameter)
            else:
                print('Button has no action...')
class Label():
    def __init__(self, text='None', pos=(10,10)):
        self.text = text
        self.pos = pos
        self.tSurface = mFont.render(text, False, (0, 0, 0))
        self.rect = (self.pos[0], self.pos[1],self.tSurface.get_width(), self.tSurface.get_height())

    def draw(self, surface):
        #rect(surface, (0,0,0), self.rect)

        surface.blit(self.tSurface, self.pos)

    def checkPress(self, pos):
        pass

class Menu():
    def __init__(self, app=None):
        pygame.font.init()
        self.size = (300,400)
        self.surface = pygame.surface.Surface(self.size)
        global mFont
        mFont = pygame.font.SysFont('Arial', 20)
        self.pos = (10,10)
        self.app = app
        self.objects = []





    def mainMenuSetup(self, gameDisconnect=False):
        def mainMenu():
            self.objects = []
            self.objects.append(Label(text='Connected!!!'))
            self.objects.append(Button(text='Create room', pos=(10,40), action=createRoom))
            self.objects.append(Button(text='Join room', pos=(10,70), action=request_joinable_rooms))
            self.reDraw()

        def createRoom():
            self.app.client.createRoom()
            self.objects = []
            self.app.start_empty_room()

        def request_joinable_rooms():
            self.roomC = 0
            self.objects = []
            self.objects.append(Label(text='Joinable rooms:'))
            self.app.client.request_joinable_rooms()

            pos = (self.surface.get_width() - 75, self.surface.get_height() - 30)
            self.objects.append(Button(text='Refresh', pos=pos, action=request_joinable_rooms))
            pos = (self.surface.get_width() - 125, self.surface.get_height() - 30)
            self.objects.append(Button(text='Back', pos=pos, action=mainMenu))
            self.reDraw()

        def actionConnect():
                print('Connecting to server....')
                self.app.client.connect()
                mainMenu()

        if gameDisconnect:
            mainMenu()
        else:
            self.objects.append(Button(text='Connect', action=actionConnect))

        self.reDraw()

    def draw(self, surface):
        surface.blit(self.surface, self.pos)

    def addNewRoomToJoin(self, name='roomUnknown'):

        def joinRoom(room):
            print('Attempting to join room ', room)
            self.app.client.requestJoinRoom(room)

        self.roomC += 1
        self.objects.append(Button(text=name, pos=(10,30 * self.roomC), action=joinRoom, parameter=name))
        self.reDraw()

    def reDraw(self):
        self.surface.fill((255,255,255))
        for object in self.objects:
            object.draw(self.surface)

    def eventMouseUp(self, pos):

        pos = (pos[0] - self.pos[0], pos[1] - self.pos[1])
        for object in self.objects:
            object.checkPress(pos)

    def update(self):
        pass
