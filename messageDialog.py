import pygame
from pygame.draw import rect
from pygame.locals import *

class MessageDialog:
    def __init__(self, app=None, caption='MessageDialog', text='Message', size=(400,130), canClose=True):
        self.app = app

        pygame.font.init()
        self.mFont = pygame.font.SysFont('Arial', 20)
        self.size = size
        self.canClose = canClose
        self.pos = (self.app.size[0] / 2 - (self.size[0] / 2), self.app.size[1] / 2 - (self.size[1] / 2))
        self.surface = pygame.surface.Surface(self.size)

        self.sCaption = self.mFont.render(caption, False, (0, 0, 0))
        self.sText = self.mFont.render(text, False, (0, 0, 0))

        self.reDraw()

    def draw(self, surface):
        surface.blit(self.surface, self.pos)

    def reDraw(self):
        self.surface.fill((180,180,230))
        rect(self.surface, (120,120,180), (0,0,self.size[0], 26))
        self.surface.blit(self.sCaption, (3,2))
        pos = (self.size[0] / 2 - (self.sText.get_width() / 2), self.size[1] / 2 - (self.sText.get_height() / 2))
        self.surface.blit(self.sText, pos)

    def close(self):
        self.app.objects.remove(self)

    def eventMouseUp(self, pos):
        if self.canClose:
            self.close()
        return True

    def update(self):
        self.app.objects.remove(self)
        self.app.objects.append(self)
